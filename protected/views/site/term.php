<?php
$this->pageTitle = 'GigaDB - Terms of use';
//echo $this->renderInternal('Yii::app()->basePath'.'/../files/html/about.html');
?>
<div class="content">
    <div class="container">
        <section class="page-title-section">
            <div class="page-title">
                <ol class="breadcrumb pull-right">
                    <li><a href="/">Home</a></li>
                    <li class="active">Terms of use</li>
                </ol>
                <h4>Terms of use</h4>
            </div>
        </section>
<section style="margin-bottom: 15px;">
    <div>
        <ul class="nav nav-tabs nav-border-tabs" role="tablist">
            <li id="lipolicies" role="presentation" class="active"><a href="#policies" aria-controls="policies"
                                                                      role="tab" data-toggle="tab">GigaScience Database
                    (<em>GigaDB</em>) Use Policies</a></li>
            <li id="liprivacy" role="presentation" id="privacytab"><a href="#privacy" aria-controls="privacy" role="tab"
                                                                      data-toggle="tab">Privacy</a></li>
            <li id="liinformation" role="presentation"><a href="#information" aria-controls="information" role="tab"
                                                          data-toggle="tab">Collection of Web-traffic Information</a>
            </li>
            <li id="lipersonal" role="presentation"><a href="#personal" aria-controls="personal" role="tab"
                                                       data-toggle="tab">Personal Data</a></li>
        </ul>
    </div>
</section>
<section>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="policies">
            <h4 style="color: #099242; margin-bottom: 10px;">General</h4>
            <div class="subsection">
                <p>We have a commitment to Open Science by freely providing an online database, data and services
                    relating to data contributed from biological and biomedical science experiments by authors and
                    users. We impose no additional restrictions on access to, or use of, the data provided beyond those
                    asserted by the data depositors. </p>
                <p>Datasets associated with GigaScience journal articles are specifically released under a Creative
                    Commons CC0 waiver unless stated otherwise. While <a
                            href="http://creativecommons.org/about/cc0">CC0</a> waives any legal requirement for
                    attribution, crediting the source of research findings and data are cultural norms and expected
                    etiquette for science. In recognition of the extensive effort that underlies these projects, we ask
                    that users appropriately acknowledge the use of any dataset(s). To aid researchers to find, access,
                    and reuse data, we issue citable digital object identifiers for each dataset. Our recommended format
                    for a data citation of each dataset is listed on each page. This is in accordance with the <a
                            href="https://doi.org/10.25490/a97f-egyk">Data Citation Principles</a>. The Open Science
                    policies and practices are only sustainable if scientific credit is generated for all parties
                    involved, and by making dataset citation details available on each dataset we are assisting in
                    developing a global research environment that rewards data sharing.</p>
                <p>Users submitting data agree to not upload viruses or malware, and all users agree not to upload,
                    post, transmit, distribute or otherwise publish on or to the system any materials that contain a
                    software virus or other harmful component.</p>
                <p>We are not liable to you or third parties claiming through you, for any loss or damage, including the
                    consequences of any discontinuity of service.</p>
                <p>While we will retain our commitment to Open Science, we reserve the right to update these Terms of
                    Use. When alterations are inevitable, we will attempt to give reasonable notice of any changes by
                    placing a notice on our website, but you may wish to check each time you use the website. The date
                    of the most recent revision will appear on this, Terms of Use page.</p>
                <p>Any questions or comments concerning these Terms of Use can be addressed to the administrator via
                    emailing <a href="mailto:database@gigasciencejournal.com">database@gigasciencejournal.com</a>.</p>
            </div>
            <h4 style="color: #099242; margin-bottom: 10px;">GigaDB Services</h4>
            <div class="subsection">
                <p>We provide these data in good faith, but make no warranty, expressed or implied, nor assume any legal
                    liability or responsibility for any purpose for which they are used.</p>
                <p>All feedback provided to us on our online services will be treated as non-confidential unless the
                    individual or organisation providing it states otherwise.</p>
                <p>Any attempt to use our online services to a level that prevents, or looks likely to prevent, us
                    providing services to others, will result in that use/user being blocked. We will attempt to contact
                    the user to discuss their needs and how (and if) these can be met from other sources.</p>
                <p>Some of the data provided from external sources may be subject to third-party constraints. Users are
                    solely responsible for establishing the nature of and complying with any such intellectual property
                    restrictions. Any software made available for download through our web pages (either directly or
                    from third party repositories like <a href="https://github.com/">GitHub</a>) will have its own
                    individual license agreement that supersedes the default CC0 waiver.</p>
            </div>


            <h4 style="color: #099242; margin-bottom: 10px;">Depositor agreement</h4>
            <div class="subsection">
                <p>As an Open Data repository, depositors must make sure everyone can legally reuse the data they
                    deposit and confirm that it is their own work. Co-authorship must be credited, and third party
                    materials created by someone else must have the necessary permissions and attribution to allow
                    sharing in this manner. Publication irrevocably grants anyone the right to use this work under the
                    <a href="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons Zero Waiver</a> which
                    releases all rights, like public domain. We agree to include expression of wishes as to their
                    specific licenses but we will not restrict access to data files based on those licenses. We require
                    that all files hosted on our server are given an OSI or similarly open license, if the authors do
                    not expressly provide one, the file(s) will be assumed to be under the most permissive <a
                            href="https://creativecommons.org/publicdomain/zero/1.0/">CC0 copyright waiver</a>.
                </p>
            </div>

            <h4 style="color: #099242; margin-bottom: 10px;">A note about BGI Data</h4>
            <div class="subsection">
                <p>As one of the world’s largest biological data producers, the BGI’s goal is to maximize the use of its
                    data by providing it to the research community in a timely manner. At the same time, BGI recognizes
                    the need for researchers to be appropriately credited for their scientific contribution and
                    investment in data generation. It is therefore expected that all researchers both honor agreements
                    in line with the <a href="http://www.genome.gov/10506537">Fort Lauderdale</a> and the <a
                            href="http://www.nature.com/nature/journal/v461/n7261/full/461168a.html">Toronto
                        International Data Release Workshop</a> data sharing principles and appropriately acknowledge
                    the contributions of others.</p>
                <p>Accordingly, raw data such as individual sequence read traces are submitted to the relevant database
                    as soon as they have exited BGI quality control pipelines. Whole genome sequence assemblies are
                    released as soon as possible following appropriate quality analysis. Our repository contains draft
                    versions of genome sequence assemblies, and we ask that you understand that these represent
                    preliminary data, subject to omissions and errors. In addition, whole genome assemblies are likely
                    to change upon the availability of new data, and our website will document new assembly versions as
                    they are released.</p>
                <p>In recognition of the extensive effort that underlies these projects, we ask that you appropriately
                    acknowledge the use of any preliminary data. To aid researchers to find, access, and reuse data, we
                    have issued citable digital object identifiers for each dataset. Our recommended format for a data
                    citation of each dataset is listed on each page. This recommendation is in accordance with the
                    adopted guidelines by the genome sequencing community in a statement of principles for the
                    distribution and use of large-scale sequencing data: <a
                            href="http://www.wellcome.ac.uk/About-us/Publications/Reports/Biomedical-science/WTD003208.htm">Community
                        Resource Projects</a> and the resulting <a
                            href="http://www.genome.gov/page.cfm?pageID=10506537">NHGRI policy statement</a>. If you
                    have any questions regarding the use of this data, please contact us at <a
                            href="mailto:database@gigasciencejournal.com">database@gigasciencejournal.com</a>. In line
                    with these recognized research norms, we request that you contact the relevant dataset contact
                    before publishing analyses of the sequences on a genome scale. We welcome collaborative interaction
                    to provide the community with improved whole genome analyses and annotations.</p>
            </div>

            <h4 style="color: #099242; margin-bottom: 10px;">Human data</h4>
            <div class="subsection">
                <p>Although all data is released under the most open licenses possible the user agrees to not use data
                    and/or metadata , alone or in combination with other data, to identify any individual or entity that
                    has been anonymized. If you inadvertently discover the identity of any patient, individual or
                    entity, then (a) You agree that you will make no use of this knowledge, (b) that you will notify us
                    (<a href="mailto:database@gigasciencejournal.com">database@gigasciencejournal.com</a>) of the
                    incident, and (c) that you will inform no one else of the discovered identity.</p>
            </div>


            <h4 style="color: #099242; margin-bottom: 10px;">Disclaimer</h4>
            <div class="subsection">
                <p>Some of the data provided from external sources may be subject to third-party constraints. Users are
                    solely responsible for establishing the nature of and complying with any such intellectual property
                    restrictions.</p>
                <p>We provide this data in good faith, but make no warranty, expressed or implied, nor assume any legal
                    liability or responsibility for any purpose for which they are used.
                </p>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="privacy">
            <p><em>GigaDB</em> has implemented appropriate technical and organisational measures to ensure a level of
                security which we deem appropriate, taking into account the categories of data we collect and the way we
                process it.</p>
            <p>For the purpose of monitoring and improving online services, planning and scientific review,
                <em>GigaDB</em> will keep its own records of usage and may use services provided by other organisations.
                <em>GigaDB</em> may make information about the total volume of usage of particular software or data
                available to third party organisations who supply the software or databases without details of any
                individual’s use.</p>
            <p>In interacting with us through our website, you may choose to give us personal data. We will keep your
                personal data confidential and use it for purposes connected to our mission. </p>
            <p><em>GigaDB</em> will hold your personal data on our systems for as long as is considered necessary for
                the purpose(s) for which you provided us with your details, or as long as required by applicable
                legislation. If you cancel your user account we will ensure that your personal information is either
                deleted or will no longer be visible to others.</p>
            <p>When you contribute scientific data to a database through our website or other submission tools, this
                information will be released at a time and in a manner consistent with the scientific data publication
                and we may store it permanently.</p>
            <p>If you post or send offensive, inappropriate or objectionable content anywhere on or to our websites or
                otherwise engage in any disruptive behaviour on any of our services, we may use your personal
                information to stop such behaviour. Where we reasonably believe that you are or may be in breach of any
                applicable laws, we may use your personal information to inform relevant third parties about the content
                and your behaviour.</p>
        </div>
        <div role="tabpanel" class="tab-pane" id="information">
            <p><em>GigaDB</em> will record the visits to the website by using cookies and page tagging without
                collecting any personal identifiable information of users. A cookie can be used to identify a computer,
                it is not used to collect any personal information. In other words, it does not have the function of
                identifying an individual user of the website. Cookies are used to collect statistics about the number
                of visits of users to <em>GigaDB</em> and the users’ preference of websites and online services offered.
                You may choose to inactivate your browser’s cookies. If you inactivate the cookies, you will not be able
                to use some of the functions of <em>GigaDB</em>.</p>
        </div>
        <div role="tabpanel" class="tab-pane" id="personal">
            <p>The Personal Data we may collect from you could include:</p>
            <ul class="content-text">
                <li>Name</li>
                <li>Email address</li>
                <li>Affiliated institute</li>
                <li>Connection information (e.g. IP address, web cookies, host information, approximate host location,
                    pages visited, services used)
                </li>
            </ul>
            <p>We may use your Personal Data:</p>
            <ul class="content-text">
                <li>To provide you with information about our services, activities or our online content by offering to
                    subscribe you to newsletters, publications, event announcements, etc.
                </li>
                <li>To personalise the way our web content is presented to you</li>
                <li>To use IP addresses to identify your location for statistical reporting purposes and if necessary to
                    block disruptive use
                </li>
                <li>To analyse and improve our websites by examining statistical usage information</li>
                <li>To generate statistical reports as to how our website and services are being used</li>
            </ul>
            <p>We may disclose your Personal Data:</p>
            <ul class="content-text">
                <li>To service providers processing your information on our behalf or as part of scientific
                    collaborations which are required to keep your information confidential
                </li>
                <li>In statistical reports to external organisations (e.g. funding bodies or scientific collaborators)
                    or appointed review committees
                </li>
                <li>In forming part of the scientific record of data submitted to our data archives</li>
                <li>To your employer, university, law enforcement or other government bodies, in exceptional
                    circumstances where your activity is disruptive or may be illegal under your local laws
                </li>
            </ul>
            <p>We may use your Personal Data to contact you:</p>
            <ul class="content-text">
                <li>When you have asked us to as part of a specific service request (e.g. password reset, help desk
                    request, voicemail message)
                </li>
                <li>In relation to any contribution (e.g. data submission or annotation) that you have provided to our
                    archives
                </li>
                <li>To invite your voluntary participation in surveys about our services</li>
                <li>When you have opted in to receive further correspondence for scientific marketing and outreach
                    purposes (e.g. newsletters)
                </li>
            </ul>
            <p>We will NOT sell, give or share your personal data to third parties for any other reasons.</p>
        </div>
    </div>
</section>


</section>


</div>
</div>
<script type="text/javascript">


    $(document).ready(function () {
        if (location.hash != null && location.hash != "") {
            $('ul li').removeClass('active');
            $('div' + '.tab-pane').removeClass('active');
            var variableli = location.hash;
            $(location.hash).addClass('active');
            $(variableli.replace('#', '#li')).addClass('active');
        }

    });

</script>
