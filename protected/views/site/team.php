<div class="content">
    <div class="container">
        <section>
            <div class="page-title">

                <h4>Our team</h4>
            </div>
        </section>
        <section>
            <div class="row" style="margin: 0px -8px;">
                <table class="table member-table">
                    <tbody>
                    <tr>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/chunts.jpg">
                                <div class="caption">
                                    <h3>Chris Hunter<br><small>Lead BioCurator</small></h3>
                                    <p>With a background in Genetics/Genomics, non-coding RNA, Cancer genetics, met
                                        agenomics and a long career in BioCuration our lead biocurator heads up the team responsible for archiving the data and
                                        metadata in GigaDB..</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/chrisa.jpg">
                                <div class="caption">
                                    <h3>Chris Armit<br><small>Data Scientist</small></h3>
                                    <p>I am a Developmental Biologist with a strong interest in image informatics a
                                        nd next-gen imaging</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/marya.jpg">
                                <div class="caption">
                                    <h3>Mary Ann Tuli<br><small>Data Editor</small></h3>
                                    <p>I have worked as a BioCurator for over 20 years, first with EMBL-Bank and
                                        then as the curator of genetic data for the model organism database, WormBa
                                        se.</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/rija.jpg">
                                <div class="caption">
                                    <h3>Rija Ménagé<br><small>Principal Software Engineer</small></h3>
                                    <p>Crafting software since 1999.</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/hongling.png">
                                <div class="caption">
                                    <h3>HongLing Zhou<br><small>Junior Editor</small></h3>
                                    <p>Aiding in assessing and coordinating submission of scientific manuscripts an
                                        d data, and handling all of the GigaScience social media accounts in China</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/image-coming-soon.jpg">
                                <div class="caption">
                                    <h3>Qi Chen<br><small>Senior secretary</small></h3>
                                    <p></p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/image-coming-soon.jpg">
                                <div class="caption">
                                    <h3>Hans Zauner<br><small>Assistant Editor</small></h3>
                                    <p>Hans is Assistant Editor at GigaScience and also writes for Laborjournal. Ma
                                        ny years ago, Hans did research in evolutionary and developmental biology. He clipped fish fins and handled a laser bea
                                        m to shoot at reproductive organs of worms. Now in safe distance from any lab bench, he works on various science commun
                                        ication projects.</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/nicole.png">
                                <div class="caption">
                                    <h3>Nicole Nogoy<br><small>Editor</small></h3>
                                    <p>Nicole has a PhD in Natural Sciences (Cardiovascular Molecular Medicine) fro
                                        m the University of Goettingen, Germany and obtained her BSc Hons in Biochemistry & Molecular Biology and Physiology fr
                                        om Victoria University of Wellington, New Zealand. Nicole has over a decade of experience in the STM publishing industr
                                        y – most notably having launched and been Managing Editor of Genome Medicine, as well as the Commissioning Editor of se
                                        veral review journals for the Future Science Group. Nicole is now Editor with GigaScience having joined the team in 201
                                        2. She is also an Open Science, Open Data and reproducible research advocate.</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/peter.png">
                                <div class="caption">
                                    <h3>Peter Li<br><small>Lead Data Manager</small></h3>
                                    <p></p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/kencho.png">
                                <div class="caption">
                                    <h3>Ken Cho<br><small>Systems Programmer Analyst</small></h3>
                                    <p>Bioinformatics graduate from The Chinese University of Hong Kong.</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/scott.png">
                                <div class="caption">
                                    <h3>Scott Edmunds<br><small>Executive Editor</small></h3>
                                    <p>Executive Editor for GigaScience journal. Having worked in Open Access publi
                                        shing for over a decade, his before that worked as a cancer molecular biologist in London and Lyon. Based in the Hong K
                                        ong office, as a self-proclaimed data nerd, outside of his day job he is Executive Committee member for Open Data Hong
                                        Kong and runs citizen science projects such as Citizenscience.Asia and the Bauhinia Genome project. Also teaching Data
                                        Management at HKU.</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="thumbnail">
                                <img src="../images/team/laurie.png">
                                <div class="caption">
                                    <h3>Laurie Goodman<br><small>Editor-in-Chief</small></h3>
                                    <p>Laurie received a BSc and MSc from Stanford University in 1986, and a PhD in
                                        Biochemistry and Molecular Biology from the University of Chicago in 1991. During her graduate work, she published a n
                                        ovel, A Spell of Deceit, with Del Rey Books. She completed a postdoctoral fellowship at the University of Colorado at B
                                        oulder then left the bench in 1995 to work as Assistant Editor at Nature Genetics. In 1997, she moved to Cold Spring Ha
                                        rbor Laboratory to become the founding Editor of Genome Research and Managing Editor of Learning & Memory. In 2006, she
                                        served as the Director for the Broad Institute Museum during its incipient year. In addition to her GigaScience role,
                                        she carries out manuscript writing seminars and owns the USA-based company Goodman Writing & Editing, which provides hi
                                        gh-level editing of scientific manuscripts and grants, with a specialty in editing manuscripts from non-native English
                                        speakers.</p>
                                </div>
                            </div>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>